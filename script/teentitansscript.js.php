//Totally stolen from CAPYGAMES, go to their website: www.gifsanity.com
// Tweakables
var imageHostPath = "";

var emptyImage = "images/misc/empty.gif";


var tweetImage = "images/misc/tweet.gif";
var soundOnImage = "images/misc/soundOn.gif";
var soundOffImage = "images/misc/soundOff.gif";
var soundOnIcon = "images/misc/icon_sound_on.png";
var soundOffIcon = "images/misc/icon_sound_off.png";
var bandcampImage = "images/misc/6955_bandcamp_linky.gif";

var tweetUrl = "https://twitter.com/intent/tweet?text=BEHOLD%20THE%20WALL%20OF%20%23GIFSANITY%20(O_O)%20%0Ahttp%3A%2F%2Fgifsanity.com%20%23SuperTimeForce%20%23Kookabunga!&related=CAPYGAMES";
var storeUrl = "http://supertimeforce.com/buy-it/";
var bandcampUrl = "http://6955.bandcamp.com";

var storeFrequency = { between: 10, and: 20, chancesOfSucessive: 0.25 };
var tweetFrequency = { between: 50, and: 60 };
var soundOnFrequency = { between: 60, and: 70 };
var bandcampFrequency = { between: 50, and: 60 };

// ---------------


// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

function ImageGenerator(url, onClickHandler, imageList, everyStartRange, everyEndRange, chancesOfSucessive)
{
	this.url = url;
	this.onClickHandler = onClickHandler;
	this.imageList = imageList;
	this.randomInterval = everyEndRange - everyStartRange;
	this.chancesOfSucessive = chancesOfSucessive;
	this.randomBase = everyStartRange;
	this.unseen = [];

	this.refreshUnseen = function()
	{
		for (var i = 0; i < this.imageList.length; i++)
			this.unseen.push(i);
	};
	this.tryShow = function() 
	{ 
		this.showIn--;
		if (this.showIn <= 0)
			return true;
		return false;
	};
	this.refreshShowIn = function()
	{
		this.showIn = Math.floor(Math.random() * this.randomInterval) + this.randomBase;
		if (Math.random() < this.chancesOfSucessive)
			this.showIn = 1;
	};
	this.getElement = function()
	{
		this.refreshShowIn();
		if (this.unseen.length == 0)
			this.refreshUnseen();
		var index = Math.floor(Math.random() * this.unseen.length)
		var image = this.imageList[this.unseen[index]];
		this.unseen.remove(index);

		var jqEle = null;

		if (onClickHandler != null)
		{
			// lolhax
			if (isSoundPlaying)
				image = soundOffImage;
		}		

		if (this.url != null)
			jqEle = $("<gif><a href='" + this.url + "'><img src='" + imageHostPath + image + "' /></a></gif>");
		else
			jqEle = $("<gif><img src='" + imageHostPath + image + "' /></gif>");

		if (onClickHandler != null)
		{
			jqEle.click(function() { onClickHandler(); });

			// lolhax
			jqEle.addClass("soundToggle");
		}

		return jqEle;
	};

	this.showIn = Math.floor(Math.random() * (this.randomInterval + this.randomBase));
}

var isSoundPlaying = false;

function toggleSound()
{
	if (isSoundPlaying)
	{
		$("audio")[0].pause();
		$(".soundToggle img.unloaded").attr("old-src", imageHostPath + soundOnImage);
		$(".soundToggle img:not(.unloaded)").attr("src", imageHostPath + soundOnImage);
		$("audioicon").css("background-image", "url(../" + imageHostPath + soundOnIcon + ")");
	}
	else
	{
		$("audio")[0].play();
		$(".soundToggle img.unloaded").attr("old-src", imageHostPath + soundOffImage);
		$(".soundToggle img:not(.unloaded)").attr("src", imageHostPath + soundOffImage);
		$("audioicon").css("background-image", "url(../" + imageHostPath + soundOffIcon + ")");
	}

	isSoundPlaying = !isSoundPlaying;
}
	

var TeenTitans = ['images/teentitans/TT00.gif', 'images/teentitans/TT01.gif', 'images/teentitans/robin.gif', 'images/teentitans/robin_starfire.gif', 'images/teentitans/teen_titans.gif', 'images/teentitans/TT02.gif', 'images/teentitans/TT03.gif', 'images/teentitans/TT04.gif', 'images/teentitans/TT05.gif', 'images/teentitans/TT06.gif', 'images/teentitans/TT07.gif', 'images/teentitans/TT08.gif', 'images/teentitans/TT09.gif', 'images/teentitans/TT10.gif', 'images/teentitans/TT11.gif', 'images/teentitans/TT12.gif', 'images/teentitans/TT13.gif'];

	
var imageGenerators = [new ImageGenerator(null, null, TeenTitans, 10, 20, 0) ];

var container = $('gifsanity');
var body = $("body");
var audioicon = $("audioicon");
var win = $(window);
var maxBatchHeight = win.height();
var lastWinWidth = 0;
var scrollSpeed = 0;
var destinationScrollSpeed = null;
var isScrolling = true;

function appendImage(toBatch) 
{
	var newImg = null;

	for (var i = 0; i < imageGenerators.length; i++)
	{
		var ig = imageGenerators[i];
		if (ig.tryShow() && newImg == null)
			newImg = ig.getElement();
	}

	if (toBatch == null)
		toBatch = container;

	toBatch.append(newImg);
	return newImg;
}

function rebatchImages(light) 
{
	if (Math.floor(lastWinWidth / 200) != Math.floor(win.width() / 200))
	{
		lastWinWidth = win.width();

		// first unbatch them
		var images = $("gif").detach();
		$("batch").remove();

		// rebatch them
		var curBatch = $("<batch />");
		container.append(curBatch);

		images.each(function(index, domEle) {
			var jqEle = $(domEle);

			// force unloaded
			if (!light)
			{
				var jqImage = jqEle.find("img");

				if (!jqImage.hasClass("unloaded"))
				{
			    	jqImage.attr("old-src", jqImage.attr("src"));
			    	jqImage.attr("src", emptyImage);
			    	jqImage.addClass("unloaded");
	    		}
	    	}

			curBatch.append(jqEle);

			if (curBatch.height() > maxBatchHeight) {
				jqEle.detach();
				curBatch = $("<batch />");
				container.append(curBatch);
				curBatch.append(jqEle);
			}
		});
	}
}

function detectClosestBatches() 
{
	var scrollTop = win.scrollTop();
	var winHeight = win.height();

	$("batch").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
		if (Math.abs(jqEle.offset().top - scrollTop) < winHeight * 1.5)
			jqEle.addClass("close");
		else
			jqEle.removeClass("close");
	});
}

function fillImages() 
{
	// fill the screen
	var lastAdded = null;
	while (container.height() < win.height() * 2 + 200)
		lastAdded = appendImage();
	if (lastAdded)
		lastAdded.remove();
}

function autoScroll() 
{
	if (destinationScrollSpeed && scrollSpeed < destinationScrollSpeed)
	{
		scrollSpeed = scrollSpeed + (destinationScrollSpeed - scrollSpeed) * 0.025;
		if (Math.abs(scrollSpeed - destinationScrollSpeed) < 0.001)
		{
			scrollSpeed = destinationScrollSpeed;
			destinationScrollSpeed = null;
		}
	}

    window.scrollBy(0, Math.max(scrollSpeed, 1.0));
    if (isScrolling)
    	setTimeout(autoScroll, (16 + 2.0 / 3) * (1.0 / Math.min(scrollSpeed, 1.0)));
}

function startScroll()
{
	isScrolling = true;
	destinationScrollSpeed = 1;
	scrollSpeed = 0;
	autoScroll();
}

function fillImageBatch() 
{
	// detect scroll-to-bottom
	var lastAdded = null;
	var curBatch = null;
	while (win.scrollTop() + win.height() * 2 + 200 > container.height())
	{
		if (curBatch == null)
			curBatch = $("batch:last");

		if (curBatch.height() > maxBatchHeight) 
		{
			curBatch = $("<batch />");
			container.append(curBatch);
		}

		lastAdded = appendImage(curBatch);
	}
	if (lastAdded)
	{
		lastAdded.remove();
		if (curBatch.children().length == 0)
			curBatch.remove();
	}

	// remove the ones that are not in view (only from the 3 batches closest to the screen)
	$("batch.close img:not(.unloaded)").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
	    var viewport = { top : win.scrollTop() };
	    viewport.bottom = viewport.top + win.height();
	    var bounds = jqEle.offset();
	    bounds.bottom = bounds.top + jqEle.outerHeight();
	    if (viewport.bottom + win.height() < bounds.top || viewport.top - win.height() > bounds.bottom)
	    {
	    	jqEle.attr("old-src", jqEle.attr("src"));
	    	jqEle.attr("src", emptyImage);
	    	jqEle.toggleClass("unloaded");
	    }
	});

	// reload the ones that are
	$("batch.close img.unloaded").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
	    var viewport = { top : win.scrollTop() };
	    viewport.bottom = viewport.top + win.height();
	    var bounds = jqEle.offset();
	    bounds.bottom = bounds.top + jqEle.outerHeight();
	    if (viewport.bottom + win.height() > bounds.top && viewport.top - win.height() < bounds.bottom)
	    {
	    	jqEle.attr("src", jqEle.attr("old-src"));
	    	jqEle.removeAttr("old-src");
	    	jqEle.toggleClass("unloaded");
	    }
	});	
}

win.scroll(function() 
{ 
	detectClosestBatches();
	fillImageBatch();
	body.css({ backgroundPositionY: win.scrollTop() * -0.5 });
	audioicon.css("opacity", 1.0 - Math.max(Math.min((win.scrollTop() - 335.0) / (385.0 - 335.0), 1.0), 0.0));
});

win.resize(function() 
{
	maxBatchHeight = win.height();	
	rebatchImages();
	detectClosestBatches();
	fillImageBatch();
});

var keys = [];
var secretFunnies = '75,79,79,75,65,66,85,78,71,65';
var secretCrazies = '87,79,65,72,68,85,68,69';

$(document).keydown(function(e) 
{
	keys.push( e.keyCode );
	if ( keys.toString().indexOf( secretFunnies ) >= 0 )
	{
		if (isScrolling)
			scrollSpeed *= 2;
		else
		{
			isScrolling = true;
			autoScroll();
			scrollSpeed = 1;
		}
		keys = [];
	}

	if ( keys.toString().indexOf( secretCrazies ) >= 0 ) {
		applyCrazy();
		keys = [];
	}	

	if (e.keyCode == 27)
	{
		if (isScrolling)
		{
			isScrolling = false;
			scrollSpeed = 0;
			return false;
		}
	}
});

$.fn.random = function() {
 var ret = $();
 if(this.length > 0)
   ret = ret.add(this[Math.floor((Math.random() * this.length))]);

 return ret;
};

function applyCrazy() {
 if (Math.floor(Math.random() * 2) == 0){
   $('batch.close > gif img').random().addClass('flipmeX').delay(1300).queue(function() {
     $(this).removeClass('flipmeX').dequeue();
   });
 } else {
   $('batch.close > gif img').random().addClass('flipmeY').delay(1300).queue(function() {
     $(this).removeClass('flipmeY').dequeue();
   });
 }
 setTimeout(applyCrazy, Math.random() * 200);
}

$(function()
{
	fillImages();
	rebatchImages(true);
	detectClosestBatches();
	fillImageBatch();
	toggleSound();
	setTimeout(startScroll(), 1000);
});