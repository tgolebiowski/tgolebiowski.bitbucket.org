 //Totally stolen from CAPYGAMES, go to their website: www.gifsanity.com
var imageHostPath = "";

var emptyImage = "images/misc/empty.gif";


var tweetImage = "images/misc/tweet.gif";
var soundOnImage = "images/misc/soundOn.gif";
var soundOffImage = "images/misc/soundOff.gif";
var soundOnIcon = "images/misc/icon_sound_on.png";
var soundOffIcon = "images/misc/icon_sound_off.png";
var bandcampImage = "images/misc/6955_bandcamp_linky.gif";

var tweetUrl = "https://twitter.com/intent/tweet?text=BEHOLD%20THE%20WALL%20OF%20%23GIFSANITY%20(O_O)%20%0Ahttp%3A%2F%2Fgifsanity.com%20%23SuperTimeForce%20%23Kookabunga!&related=CAPYGAMES";
var storeUrl = "http://supertimeforce.com/buy-it/";
var bandcampUrl = "http://6955.bandcamp.com";

var storeFrequency = { between: 10, and: 20, chancesOfSucessive: 0.25 };
var tweetFrequency = { between: 50, and: 60 };
var soundOnFrequency = { between: 60, and: 70 };
var bandcampFrequency = { between: 50, and: 60 };
// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

function ImageGenerator(url, onClickHandler, imageList, everyStartRange, everyEndRange, chancesOfSucessive)
{
	this.url = url;
	this.onClickHandler = onClickHandler;
	this.imageList = imageList;
	this.randomInterval = everyEndRange - everyStartRange;
	this.chancesOfSucessive = chancesOfSucessive;
	this.randomBase = everyStartRange;
	this.unseen = [];

	this.refreshUnseen = function()
	{
		for (var i = 0; i < this.imageList.length; i++)
			this.unseen.push(i);
	};
	this.tryShow = function() 
	{ 
		this.showIn--;
		if (this.showIn <= 0)
			return true;
		return false;
	};
	this.refreshShowIn = function()
	{
		this.showIn = Math.floor(Math.random() * this.randomInterval) + this.randomBase;
		if (Math.random() < this.chancesOfSucessive)
			this.showIn = 1;
	};
	this.getElement = function()
	{
		this.refreshShowIn();
		if (this.unseen.length == 0)
			this.refreshUnseen();
		var index = Math.floor(Math.random() * this.unseen.length)
		var image = this.imageList[this.unseen[index]];
		this.unseen.remove(index);

		var jqEle = null;

		if (onClickHandler != null)
		{
			// lolhax
			if (isSoundPlaying)
				image = soundOffImage;
		}		

		if (this.url != null)
			jqEle = $("<gif><a href='" + this.url + "'><img src='" + imageHostPath + image + "' /></a></gif>");
		else
			jqEle = $("<gif><img src='" + imageHostPath + image + "' /></gif>");

		if (onClickHandler != null)
		{
			jqEle.click(function() { onClickHandler(); });

			// lolhax
			jqEle.addClass("soundToggle");
		}

		return jqEle;
	};

	this.showIn = Math.floor(Math.random() * (this.randomInterval + this.randomBase));
}

var isSoundPlaying = false;

function toggleSound()
{
	if (isSoundPlaying)
	{
		$("audio")[0].pause();
		$(".soundToggle img.unloaded").attr("old-src", imageHostPath + soundOnImage);
		$(".soundToggle img:not(.unloaded)").attr("src", imageHostPath + soundOnImage);
		$("audioicon").css("background-image", "url(../" + imageHostPath + soundOnIcon + ")");
	}
	else
	{
		$("audio")[0].play();
		$(".soundToggle img.unloaded").attr("old-src", imageHostPath + soundOffImage);
		$(".soundToggle img:not(.unloaded)").attr("src", imageHostPath + soundOffImage);
		$("audioicon").css("background-image", "url(../" + imageHostPath + soundOffIcon + ")");
	}

	isSoundPlaying = !isSoundPlaying;
}
	
var EdEddNEddyFilePaths = ['images/Double_D.gif', 'images/Ed.gif', 'images/Eddy.gif', 'images/EEnE_Awkward_Dance.gif', 'images/Jawbreakers!.gif', 'images/ed_edd_n_eddy/ed_edd_n_eddy.gif', 'images/ed_edd_n_eddy/ed.gif', 'images/ed_edd_n_eddy/ed2.gif', 'images/ed_edd_n_eddy/ed3.gif', 'images/ed_edd_n_eddy/edd.gif', 'images/ed_edd_n_eddy/edd2.gif', 'images/ed_edd_n_eddy/eddy_n_ed.gif', 'images/ed_edd_n_eddy/eddy.gif', 'images/ed_edd_n_eddy/eddy2.gif', 'images/Spongebob/E_EE.gif', 'images/Spongebob/Edward.gif', 'images/Double_D.gif', 'images/Ed.gif', 'images/Eddy.gif', 'images/EEnE_Awkward_Dance.gif', 'images/ed_edd_n_eddy/ed_edd_n_eddy.gif', 'images/ed_edd_n_eddy/ed.gif', 'images/ed_edd_n_eddy/ed2.gif', 'images/ed_edd_n_eddy/ed3.gif', 'images/ed_edd_n_eddy/edd.gif', 'images/ed_edd_n_eddy/edd2.gif', 'images/ed_edd_n_eddy/eddy_n_ed.gif', 'images/ed_edd_n_eddy/eddy.gif', 'images/ed_edd_n_eddy/eddy2.gif', 'images/Jawbreakers!.gif', ];
var SpongeBobFilePaths = ['images/Spongebob/bob.gif', 'images/Spongebob/bob1.gif', 'images/Spongebob/bob2.gif', 'images/Spongebob/bob3.gif', 'images/Spongebob/bob4.gif', 'images/Spongebob/bob5.gif', 'images/Spongebob/bob6.gif', 'images/Spongebob/bob7.gif', 'images/Spongebob/bob8.gif', 'images/Spongebob/bob9.gif', 'images/Spongebob/bob10.gif', 'images/NewSpongeGifs/SB00.gif', 'images/NewSpongeGifs/SB01.gif', 'images/NewSpongeGifs/SB02.gif', 'images/NewSpongeGifs/SB03.gif', 'images/NewSpongeGifs/SB04.gif', 'images/NewSpongeGifs/SB05.gif', 'images/NewSpongeGifs/SB06.gif', 'images/NewSpongeGifs/SB07.gif', 'images/NewSpongeGifs/SB08.gif', 'images/NewSpongeGifs/SB09.gif', 'images/NewSpongeGifs/SB10.gif', 'images/NewSpongeGifs/SB11.gif', 'images/NewSpongeGifs/SB12.gif', 'images/NewSpongeGifs/SB13.gif', 'images/NewSpongeGifs/SB14.gif', 'images/NewSpongeGifs/SB15.gif', 'images/NewSpongeGifs/SB16.gif', 'images/NewSpongeGifs/SB17.gif', 'images/NewSpongeGifs/SB18.gif', 'images/NewSpongeGifs/SB19.gif', 'images/NewSpongeGifs/SB20.gif', 'images/NewSpongeGifs/SB21.gif', 'images/NewSpongeGifs/SB22.gif', 'images/NewSpongeGifs/SB23.gif', 'images/NewSpongeGifs/SB24.gif', 'images/NewSpongeGifs/SB25.gif', 'images/NewSpongeGifs/SB26.gif', 'images/NewSpongeGifs/SB27.gif', 'images/NewSpongeGifs/SB28.gif', 'images/NewSpongeGifs/SB29.gif', 'images/NewSpongeGifs/SB30.gif', 'images/NewSpongeGifs/SB31.gif', 'images/NewSpongeGifs/SB32.gif', 'images/NewSpongeGifs/SB33.gif', 'images/NewSpongeGifs/SB34.gif', 'images/NewSpongeGifs/SB35.gif', 'images/NewSpongeGifs/SB36.gif', 'images/NewSpongeGifs/SB37.gif', 'images/NewSpongeGifs/SB38.gif', 'images/NewSpongeGifs/SB39.gif', 'images/NewSpongeGifs/SB40.gif', 'images/NewSpongeGifs/SB41.gif', 'images/NewSpongeGifs/SB42.gif', 'images/NewSpongeGifs/SB43.gif', 'images/NewSpongeGifs/SB44.gif', 'images/NewSpongeGifs/SB45.gif', 'images/NewSpongeGifs/SB46.gif', 'images/NewSpongeGifs/SB47.gif', 'images/NewSpongeGifs/SB48.gif', 'images/NewSpongeGifs/SB49.gif', 'images/NewSpongeGifs/SB50.gif', 'images/NewSpongeGifs/SB51.gif', 'images/NewSpongeGifs/SB52.gif', 'images/NewSpongeGifs/SB53.gif', 'images/NewSpongeGifs/SB54.gif', 'images/NewSpongeGifs/SB55.gif', 'images/NewSpongeGifs/SB56.gif', 'images/NewSpongeGifs/SB57.gif', 'images/NewSpongeGifs/SB58.gif', 'images/NewSpongeGifs/SB59.gif', 'images/NewSpongeGifs/SB60.gif', 'images/NewSpongeGifs/SB61.gif', 'images/Spongebob/bob.gif', 'images/Spongebob/bob1.gif', 'images/Spongebob/bob2.gif', 'images/Spongebob/bob3.gif', 'images/Spongebob/bob4.gif', 'images/Spongebob/bob5.gif', 'images/Spongebob/bob6.gif', 'images/Spongebob/bob7.gif', 'images/Spongebob/bob8.gif', 'images/Spongebob/bob9.gif', 'images/Spongebob/bob10.gif'];
var TMNTAll = ['images/TMNT/new/Donatello_New.gif', 'images/TMNT/new/Leonardo_New.gif', 'images/TMNT/new/Michaelangelo_New.gif', 'images/TMNT/new/Raphael_New.gif', 'images/TMNT/new/TMNT_Team_New.gif', 'images/TMNT/old/Donatello_Old.gif', 'images/TMNT/old/Leonardo_Old.gif', 'images/TMNT/old/Michaelangelo_Old.gif', 'images/TMNT/old/Raphael_Old.gif', 'images/TMNT/old/TMNT_Old.gif'];
var XMenAll = ['images/XMen/new/Magneto_New.gif', 'images/XMen/new/ProfesorX_New.gif', 'images/XMen/new/Rogue_New.gif', 'images/XMen/new/Wolverine_New.gif/', 'images/XMen/new/Xmen_New.gif', 'images/XMen/old/Magneto_Old.gif', 'images/XMen/old/ProfessorX_Old.gif', 'images/XMen/old/Rogue_Old.gif', 'images/XMen/old/Wolverine_Old.gif', 'images/XMen/old/Xmen_Old.gif'];
var AdventureTime = ['images/AdventureGifs/Time00.gif', 'images/AdventureGifs/Time01.gif', 'images/AdventureGifs/Time02.gif', 'images/AdventureGifs/Time03.gif', 'images/AdventureGifs/Time04.gif', 'images/AdventureGifs/Time05.gif', 'images/AdventureGifs/Time06.gif', 'images/AdventureGifs/Time07.gif', 'images/AdventureGifs/Time08.gif', 'images/AdventureGifs/Time09.gif', 'images/AdventureGifs/Time10.gif', 'images/AdventureGifs/Time11.gif', 'images/AdventureGifs/Time12.gif', 'images/AdventureGifs/Time13.gif', 'images/AdventureGifs/Time14.gif', 'images/AdventureGifs/Time15.gif', 'images/AdventureGifs/Time16.gif', 'images/AdventureGifs/Time17.gif', 'images/AdventureGifs/Time18.gif', 'images/AdventureGifs/Time19.gif', 'images/AdventureGifs/Time20.gif', 'images/AdventureGifs/Time21.gif', 'images/AdventureGifs/Time22.gif', 'images/AdventureGifs/Time23.gif', 'images/AdventureGifs/Time24.gif', 'images/AdventureGifs/Time25.gif', 'images/AdventureGifs/Time26.gif', 'images/AdventureGifs/Time27.gif', 'images/AdventureGifs/Time28.gif', 'images/AdventureGifs/Time29.gif', 'images/AdventureGifs/Time30.gif', 'images/AdventureGifs/Time31.gif', 'images/AdventureGifs/Time32.gif', 'images/AdventureGifs/Time33.gif', 'images/AdventureGifs/Time34.gif', 'images/AdventureGifs/Time35.gif', 'images/AdventureGifs/Time36.gif', 'images/AdventureGifs/Time37.gif', 'images/AdventureGifs/Time38.gif', 'images/AdventureGifs/Time39.gif', 'images/AdventureGifs/Time40.gif', 'images/AdventureGifs/Time41.gif', 'images/AdventureGifs/Time42.gif', 'images/AdventureGifs/Time43.gif', 'images/AdventureGifs/Time44.gif', 'images/AdventureGifs/Time45.gif', 'images/AdventureGifs/Time46.gif', 'images/AdventureGifs/Time47.gif', 'images/AdventureGifs/Time48.gif', 'images/AdventureGifs/Time49.gif', 'images/AdventureGifs/Time50.gif', 'images/AdventureGifs/Time51.gif', 'images/AdventureGifs/Time52.gif', 'images/AdventureGifs/Time53.gif', 'images/AdventureGifs/Time54.gif', 'images/AdventureGifs/Time55.gif', 'images/AdventureGifs/Time56.gif'];
var miscGifs = ['images/TeamRocket.gif', 'images/TimeToDuel.gif', 'images/Chrono_Trigger_Time_Travel.gif', 'images/Goku_Kaioken.gif', 'images/Ill_Catch_You!.gif', 'images/Link_To_The_Past.gif', 'images/Majin_Buu.gif', 'images/Piccolo.gif', 'images/PPG_Sisterly_Love.gif', 'images/Shenlong.gif', 'images/Trunks.gif', 'images/codename_knd/numbuh1and5.gif', 'images/codename_knd/numbuh3and4.gif', 'images/codename_knd/wally_kuki.gif', 'images/courage_the_cowardly_dog/courage_the_cowardly_dog.gif', 'images/courage_the_cowardly_dog/courage_the_cowardly_dog2.gif', 'images/dragon_ball_z/kid_goku.gif', 'images/pokemon/pokemon1.gif', 'images/pokemon/pokemon2.gif', 'images/pokemon/pokemon3.gif', 'images/powerpuff_girls/powerpuff_girls1.gif', 'images/powerpuff_girls/powerpuff_girls2.gif', 'images/Spongebob/batman.gif', 'images/Spongebob/beavis.gif', 'images/Spongebob/butthead.gif', 'images/Spongebob/dexter.gif', 'images/Spongebob/doug.gif', 'images/Spongebob/jBravo.gif', 'images/Spongebob/jBravo2.gif', 'images/Spongebob/pizza.gif', 'images/Spongebob/ppfgirls.gif', 'images/Spongebob/RenAndStimpy.gif', 'images/Spongebob/RenAndStimpy2.gif', 'images/Spongebob/rolf.gif', 'images/Spongebob/sailormoon.gif', 'images/Spongebob/spaceghost.gif', 'images/Spongebob/thing.gif', 'images/Spongebob/tmnt.gif', 'images/teentitans/robin.gif', 'images/teentitans/robin_starfire.gif', 'images/teentitans/teen_titans.gif', 'images/Oh..._The_Humanity.gif'];
var DBZGifs = ['images/DBZ/Anime_man.gif', 'images/DBZ/Broly_badass.gif', 'images/DBZ/DBZ_Assemble!.gif', 'images/DBZ/DBZ_newest.gif', 'images/DBZ/Dragonball_Logo.gif', 'images/DBZ/Dragonballs.gif', 'images/DBZ/Gohan_Shenlong_run.gif', 'images/DBZ/Goku_eating.gif', 'images/DBZ/Goku_vs_Freiza.gif', 'images/DBZ/Kaiyo_ken!.gif', 'images/DBZ/Krillin_Funny.gif', 'images/DBZ/Goku_Kaioken.gif', 'images/DBZ/Ill_Catch_You.gif', 'images/DBZ/kid_goku.gif', 'images/DBZ/Majin_Buu.gif', 'images/DBZ/Piccolo.gif', 'images/DBZ/Shenlong.gif', 'images/DBZ/Trunks.gif', 'images/DBZ/Piccolo_badass.gif', 'images/DBZ/Trunks_Again.gif', 'images/DBZ/Vageta_Gallek_Gun!.gif'];
	
var imageGenerators = [ new ImageGenerator(null, null, SpongeBobFilePaths, 0, 0, 0) ];

var container = $('gifsanity');
var body = $("body");
var audioicon = $("audioicon");
var win = $(window);
var maxBatchHeight = win.height();
var lastWinWidth = 0;
var scrollSpeed = 0;
var destinationScrollSpeed = null;
var isScrolling = false;

function appendImage(toBatch) 
{
	var newImg = null;

	for (var i = 0; i < imageGenerators.length; i++)
	{
		var ig = imageGenerators[i];
		if (ig.tryShow() && newImg == null)
			newImg = ig.getElement();
	}

	if (toBatch == null)
		toBatch = container;

	toBatch.append(newImg);
	return newImg;
}

function rebatchImages(light) 
{
	if (Math.floor(lastWinWidth / 200) != Math.floor(win.width() / 200))
	{
		lastWinWidth = win.width();

		// first unbatch them
		var images = $("gif").detach();
		$("batch").remove();

		// rebatch them
		var curBatch = $("<batch />");
		container.append(curBatch);

		images.each(function(index, domEle) {
			var jqEle = $(domEle);

			// force unloaded
			if (!light)
			{
				var jqImage = jqEle.find("img");

				if (!jqImage.hasClass("unloaded"))
				{
			    	jqImage.attr("old-src", jqImage.attr("src"));
			    	jqImage.attr("src", emptyImage);
			    	jqImage.addClass("unloaded");
	    		}
	    	}

			curBatch.append(jqEle);

			if (curBatch.height() > maxBatchHeight) {
				jqEle.detach();
				curBatch = $("<batch />");
				container.append(curBatch);
				curBatch.append(jqEle);
			}
		});
	}
}

function detectClosestBatches() 
{
	var scrollTop = win.scrollTop();
	var winHeight = win.height();

	$("batch").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
		if (Math.abs(jqEle.offset().top - scrollTop) < winHeight * 1.5)
			jqEle.addClass("close");
		else
			jqEle.removeClass("close");
	});
}

function fillImages() 
{
	// fill the screen
	var lastAdded = null;
	while (container.height() < win.height() * 2 + 200)
		lastAdded = appendImage();
	if (lastAdded)
		lastAdded.remove();
}

function autoScroll() 
{
	if (destinationScrollSpeed && scrollSpeed < destinationScrollSpeed)
	{
		scrollSpeed = scrollSpeed + (destinationScrollSpeed - scrollSpeed) * 0.025;
		if (Math.abs(scrollSpeed - destinationScrollSpeed) < 0.001)
		{
			scrollSpeed = destinationScrollSpeed;
			destinationScrollSpeed = null;
		}
	}

    window.scrollBy(0, Math.max(scrollSpeed, 1.0));
    if (isScrolling)
    	setTimeout(autoScroll, (16 + 2.0 / 3) * (1.0 / Math.min(scrollSpeed, 1.0)));
}

function startScroll()
{
	isScrolling = true;
	destinationScrollSpeed = 1;
	scrollSpeed = 0;
	autoScroll();
}

function fillImageBatch() 
{
	// detect scroll-to-bottom
	var lastAdded = null;
	var curBatch = null;
	while (win.scrollTop() + win.height() * 2 + 200 > container.height())
	{
		if (curBatch == null)
			curBatch = $("batch:last");

		if (curBatch.height() > maxBatchHeight) 
		{
			curBatch = $("<batch />");
			container.append(curBatch);
		}

		lastAdded = appendImage(curBatch);
	}
	if (lastAdded)
	{
		lastAdded.remove();
		if (curBatch.children().length == 0)
			curBatch.remove();
	}

	// remove the ones that are not in view (only from the 3 batches closest to the screen)
	$("batch.close img:not(.unloaded)").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
	    var viewport = { top : win.scrollTop() };
	    viewport.bottom = viewport.top + win.height();
	    var bounds = jqEle.offset();
	    bounds.bottom = bounds.top + jqEle.outerHeight();
	    if (viewport.bottom + win.height() < bounds.top || viewport.top - win.height() > bounds.bottom)
	    {
	    	jqEle.attr("old-src", jqEle.attr("src"));
	    	jqEle.attr("src", emptyImage);
	    	jqEle.toggleClass("unloaded");
	    }
	});

	// reload the ones that are
	$("batch.close img.unloaded").each(function(index, domEle) 
	{
		var jqEle = $(domEle);
	    var viewport = { top : win.scrollTop() };
	    viewport.bottom = viewport.top + win.height();
	    var bounds = jqEle.offset();
	    bounds.bottom = bounds.top + jqEle.outerHeight();
	    if (viewport.bottom + win.height() > bounds.top && viewport.top - win.height() < bounds.bottom)
	    {
	    	jqEle.attr("src", jqEle.attr("old-src"));
	    	jqEle.removeAttr("old-src");
	    	jqEle.toggleClass("unloaded");
	    }
	});	
}

win.scroll(function() 
{ 
	detectClosestBatches();
	fillImageBatch();
	body.css({ backgroundPositionY: win.scrollTop() * -0.5 });
	audioicon.css("opacity", 1.0 - Math.max(Math.min((win.scrollTop() - 335.0) / (385.0 - 335.0), 1.0), 0.0));
});

win.resize(function() 
{
	maxBatchHeight = win.height();	
	rebatchImages();
	detectClosestBatches();
	fillImageBatch();
});

var keys = [];
var secretFunnies = '75,79,79,75,65,66,85,78,71,65';
var secretCrazies = '87,79,65,72,68,85,68,69';

$(document).keydown(function(e) 
{
	keys.push( e.keyCode );
	if ( keys.toString().indexOf( secretFunnies ) >= 0 )
	{
		if (isScrolling)
			scrollSpeed *= 2;
		else
		{
			isScrolling = true;
			autoScroll();
			scrollSpeed = 1;
		}
		keys = [];
	}

	if ( keys.toString().indexOf( secretCrazies ) >= 0 ) {
		applyCrazy();
		keys = [];
	}	

	if (e.keyCode == 27)
	{
		if (isScrolling)
		{
			isScrolling = false;
			scrollSpeed = 0;
			return false;
		}
	}
});

$.fn.random = function() {
 var ret = $();
 if(this.length > 0)
   ret = ret.add(this[Math.floor((Math.random() * this.length))]);

 return ret;
};

function applyCrazy() {
 if (Math.floor(Math.random() * 2) == 0){
   $('batch.close > gif img').random().addClass('flipmeX').delay(1300).queue(function() {
     $(this).removeClass('flipmeX').dequeue();
   });
 } else {
   $('batch.close > gif img').random().addClass('flipmeY').delay(1300).queue(function() {
     $(this).removeClass('flipmeY').dequeue();
   });
 }
 setTimeout(applyCrazy, Math.random() * 200);
}

$(function()
{
	fillImages();
	rebatchImages(true);
	detectClosestBatches();
	fillImageBatch();
	toggleSound();
	setTimeout(startScroll(), 1000);
});